﻿using System;
using System.Collections;


/**
 * Classe utilisée pour générer une grille hexagonale de 36 cases
 * J'ai choisi de commenter mon code en Français mais d'écrire le code en anglais
 * pour une question de maintenabilité et de lisibilité.
 * Nabil Sadeg
 */
namespace Algorithmes {
	public class GrilleHexagonale {

		// FIELDS
		private int [,] grid = new int [7, 7];
		private bool valid;



		// Constructor
		public GrilleHexagonale () {
			valid = false;

			// Génère la grille
			generateGrid ();
		}



		/**
		 * Génère la grille de jeu
		 */
		public void generateGrid () {
			resetGrid ();
			populateGrid ();
		}



		/**
		 * Prépare la grille en mettant 0 aux cases utilisables et -1 a celles
		 * inutilisables
		 */
		private void resetGrid () {
			for (int i = 0; i < 7; i++) {
				for (int j = 0; j < 7; j++) {
					if (notUsed (i, j))
						grid [i, j] = -1;
					else
						grid [i, j] = 0;
				}
			}
		}



		/**
		 * Détermine si une case est utilisable ou non
		 * return true si la case n'est pas utilisable
		 * return false si elle est utilisable
		 */
		private bool notUsed (int x, int y) {
			if (x == 6 && y >= 4)
				return true;
			else if (x == 5 && y >= 5)
				return true;
			else if ((x == 4 && y == 6) || (x == 2 && y == 0) || (x == 3 && y == 3))
				return true;
			else if (x == 1 && y <= 1)
				return true;
			else if (x == 0 && y <= 2)
				return true;
			else
				return false;
		}



		/**
		 * Affiche la grille
		 */
		public void printGrid () {

			// Itération sur les differentes cases de la grille
			for (int i = 0; i < 7; i++) {

				// Détermine le nombre d'espace à mettre au début de la ligne
				int spaces;
				if (i < 3)
					spaces = 3 - i;
				else
					spaces = i - 3;
				for (int s = 0; s < spaces; s++)
					Console.Write ("  ");

				// Affiche les différentes cases
				for (int j = 0; j < 7; j++) {
					if (i == 3 && j == 3)
						Console.Write ("{0,3}", "");
					if (grid [i, j] != -1)
						Console.Write ("{0,3} ", grid [i, j]);
				}
				Console.WriteLine ();
			}
		}



		/**
		 * Remplie la grille
		 */
		private void populateGrid () {
			valid = true;

			// Sélectionne la case de depart
			int [] currentCoordinates = firstCell ();

			// Pour chaque nombre n...
			for (int i = 1; i <= 36; i++) {

				// ...détermine les voisins possible...
				ArrayList neighbours = findNeighbours (currentCoordinates);
				// ...sélectionne un voisin...
				currentCoordinates = pickNeighbour (neighbours);
				// ...vérifie que le voisin est valide...
				if (currentCoordinates == null) {
					valid = false;
					break;
				}
				// ... assigne n aux coordonnees
				grid [currentCoordinates [0], currentCoordinates [1]] = i;
			}
		}



		/**
		 * Sélectionne la premiere case
		 * return les coordonnées de la première case
		 */
		private int[] firstCell () {

			// Sélectionne des coordonnées aléatoirement
			Random random = new Random ();
			int x = 0;
			int y = 0;

			// Vérifie que les coordonnées sont valides
			do {
				x = random.Next (0, 7);
				y = random.Next (0, 7);
			} while (grid [x, y] != -1);

			return (new int [] { x, y });
		}



		/**
		 * Détermine les coordonnées des voisins
		 * return une liste contenant les coordonnées de tous les voisins potentiels
		 */
		private ArrayList findNeighbours (int[] coordinates) {
			ArrayList neigbours = new ArrayList ();

			// Ajoute tous les voisins possible a la liste
			neigbours.Add (new int [2] { coordinates [0], coordinates [1] - 1 });
			neigbours.Add (new int [2] { coordinates [0], coordinates [1] + 1 });
			neigbours.Add (new int [2] { coordinates [0] + 1, coordinates [1] });
			neigbours.Add (new int [2] { coordinates [0] + 1, coordinates [1] - 1 });
			neigbours.Add (new int [2] { coordinates [0] - 1, coordinates [1] });
			neigbours.Add (new int [2] { coordinates [0] - 1, coordinates [1] + 1 });
			return neigbours;
		}



		/***
		 * Sélectionne un voisin
		 * return les coordonnées du voisin sélectionné
		 */
		private int [] pickNeighbour (ArrayList neighbours) {

			// Itération sur tous les voisins disponibles 
			for (int i = neighbours.Count - 1; i >= 0; i--) {

				// Recupère les coordonnées
				int [] coordinates = (int[]) neighbours [i];
				int x = coordinates [0];
				int y = coordinates [1];

				// Si les coordonnées ne sont pas valides le voisin est supprimé
				if (x > 6 || x < 0 || y > 6 || y < 0)
					neighbours.Remove (coordinates);

				// Si la case n'est pas valide le voisin est supprimé
				else if (grid [coordinates [0], coordinates [1]] != 0)
					neighbours.Remove (coordinates);
			}

			// Si la liste est vide on retourne null
			if (neighbours.Count == 0)
				return null;

			// Sinon on choisit un voisin aleatoirement
			Random random = new Random ();
			return (int[]) neighbours [random.Next (0, neighbours.Count)];
		}



		/**
		 * Vérfifie si la grille est valide
		 * return true si la grille est valide
		 */
		public bool isValid () {
			return valid;
		}



		public int [,] getGrid () {
			return this.grid;
		}



		/**
		 * Main fonction
		 */
		public static void Main () {
			// Création d'un objet GrilleHexagonale
			GrilleHexagonale grid = new GrilleHexagonale ();

			// Génère la grille jusqu'a ce qu'elle soit valide
			while (!grid.isValid ())
				grid.generateGrid ();

			// Affiche la grille
			grid.printGrid ();
		}
	}
}
