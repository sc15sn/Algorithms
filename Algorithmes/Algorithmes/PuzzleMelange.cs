﻿using System;

/**
 * Classe utilisée pour mélanger un Puzzle 8
 * J'ai choisi de commenter mon code en Français mais d'écrire le code en anglais
 * pour une question de maintenabilité et de lisibilité.
 * Nabil Sadeg
 */
namespace Algorithmes {
	public class PuzzleMelange {

		// FIELDS
		private int [,] puzzle;
		private static readonly Random random = new Random ();
		private static readonly object syncLock = new object ();
		enum Movement { UP, DOWN, LEFT, RIGHT }; // mouvements possibles



		/**
		 * Méthode utilisée pour générer aléatoirement des nombres dans un couurt
		 * laps de temps.
		 * 
		 * min, max		les bornes inférieures et suprieures
		 * return le nombre généré.
		 */
		public static int RandomNumber (int min, int max) {
			lock (syncLock) { // synchronize
				return random.Next (min, max);
			}
		}



		/**
		 * Constructeur de PuzzleMelange
		 * prend un grille de 3 par 3 comme argument, représentant le puzzle
		 */
		public PuzzleMelange (int [,] puzzle) {
			this.puzzle = puzzle;
			mixPuzzle ();
		}



		/**
		 * Méthode utilisée pour mélanger le puzzle
		 */
		private void mixPuzzle () {
			// Choisit le nombre de fois que le Puzzle va etre melange
			int x = random.Next (10, 100);
			// Trouve la case vide
			int [] emptyCell = findEmptyCell ();

			// Mélange le pûzzle
			for (int i = 0; i < x; i++)
				emptyCell = singleMix (emptyCell);
		}



		/**
		 * Méthode utilisée pour trouver la case vide
		 * return les coordonnées de la case vide
		 */
		private int [] findEmptyCell () {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					if (puzzle [i, j] == 0)
						return (new int [2] { i, j });
				}
			}
			return null;
		}



		/**
		 * Méthode utilisée pour effectuer un mouvement
		 * 
		 * emptyCell 	coordonnées de la case vide
		 * return		coordonnées de la nouvelle case vide
		 */
		private int [] singleMix (int [] emptyCell) {

			// Choisi un mouvement aléatoire
			Movement move = (Movement) RandomNumber (0, 4);
			int x = emptyCell [0];
			int y = emptyCell [1];

			// Bouge la case en fonction du mouvement
			switch (move) {
				// DROITE
				case Movement.RIGHT:
					if ((emptyCell [0] + 1 <= 2)) {
						puzzle [x, y] = puzzle [x + 1, y];
						puzzle [x + 1, y] = 0;
						return (new int [] { x + 1, y });
					}
					break;

				// GAUCHE
				case Movement.LEFT:
					if ((emptyCell [0] - 1 >= 0)) {
						puzzle [x, y] = puzzle [x - 1, y];
						puzzle [x - 1, y] = 0;
						return (new int [] { x - 1, y });
					}
					break;

				// HAUT
				case Movement.UP:
					if ((emptyCell [1] + 1 <= 2)) {
						puzzle [x, y] = puzzle [x, y + 1];
						puzzle [x, y + 1] = 0;
						return (new int [] { x, y + 1 });
					}
					break;

				// BAS
				case Movement.DOWN:
					if ((emptyCell [1] - 1 >= 0)) {
						puzzle [x, y] = puzzle [x, y - 1];
						puzzle [x, y - 1] = 0;
						return (new int [] { x, y - 1 });
					}
					break;
			}
			return emptyCell;
		}



		/**
		 * Méthode utilisée pour afficher le Puzzle
		 */
		public void printPuzzle () {
			for (int x = 0; x < 3; x++) {
				for (int y = 0; y < 3; y++) {
					Console.Write (puzzle [x, y]);
				}
				Console.WriteLine ();
			}
		}




		public int [,] getPuzzle () {
			return this.puzzle;
		}




		/**
		 * Main fonction
		 */
		public static void Main () {

			int [,] puzzle = new int [3, 3] { {1, 2, 3 },
												{4, 5, 6},
												{7, 8, 0}};

			PuzzleMelange melangeur = new PuzzleMelange (puzzle);
			melangeur.printPuzzle ();
		}
	}
}
