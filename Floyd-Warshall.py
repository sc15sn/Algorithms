"""
Find all-pairs shortest paths on a graph.
"""

import math

def floyd_washall(Array):
	n = len(Array)
	for k in range(n):
		for i in range(n):
			for j in range(n):
				if Array[i][j] > Array[i][k] + Array[k][j]:
					Array[i][j] = Array[i][k] + Array[k][j]
		for row in Array:
			print(row)
		print()
	for row in Array:
		print(row)

inf = float("inf")

Array = [[0,4,inf,-2,inf,inf],
		[4,0,3,inf,inf,inf],
		[inf,3,0,inf,inf,-2],
		[inf,inf,inf,0,3,inf],
		[inf,-1,inf,3,0,4],
		[inf,inf,inf,inf,4,0]]

floyd_washall(Array)