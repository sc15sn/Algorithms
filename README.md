# Sorting algorithms  
* Merge-sort
* Quick-sort
* Bubble-sort
* Heap-sort
* Insertion-sort
* Selection-sort

# Other
* Floyd-Warshall
* Matrix Chain Multiplication

# Algorithmes contains C# Algorithmes with comments in French
* GrilleHexagonale to create a random hexagonal grid with 36 cells
* PuzzleMelange to mix a 8 Puzzle randomly