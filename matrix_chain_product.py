def matrix_chain_product(d):
	# Initialise array with zeros
	N = []
	for x in range(len(d)):
		# Conctruct row
		row = []
		for y in range(len(d)):
			row.append([0,0])
		N.append(row)

	for delta in range(1, len(d)-1): 
		for i in range(1, len(d)-delta):
			k = i+delta
			temp = []
			for j in range(i,k):
				a = N[i][j][0] + d[i-1] * d[j] * d[k] + N[j+1][k][0]
				temp.append(a)

			m = min(temp)
			index = temp.index(m) + i
			N[i][k] = [m,index]

	for n in range(1,len(d)-1):
		print(N[n][2:])


d = [3,4,5,6,5,3,2]		
matrix_chain_product(d)