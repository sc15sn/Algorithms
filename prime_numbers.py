def prime(n):
    result = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            result.append(d)
            n //= d
        d += 1
    if n > 1:
       result.append(n)
    return result

while True:
	x = input("Enter a number or exit to exit: ")
	
	if type(x) == int:
		print("The prime factors of {} are {}".format(x, prime(int(x))))
	else:
		print("{} is not a valid input!".format(x))
